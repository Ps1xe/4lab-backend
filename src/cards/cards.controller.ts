import { Controller, Get, Post, Patch, Delete, Param, Body } from '@nestjs/common'
import { CardService } from './cards.service'
import { Card } from './entities/card.entity';


@Controller()
export class CardControler {
    constructor(private cardService: CardService) { }

    @Get('/cards')
    getAllCards(): Card[] {
        return this.cardService.getCards();
    }

    @Get('/cards/:id')
    getOneCard(@Param('id') id): Card {
        return this.cardService.getCard(id);
    }

    @Get('/columns/:id/cards')
    getAllCardsOnColumn(@Param('id') id): Card[] {
        return this.cardService.findCardsOnColumn(id);
    }

    @Post('/cards')
    createCard(@Body() dto): Card {
        return this.cardService.createCard(dto);
    }
    @Patch('/cards/:id')
    updateCard(@Param('id') id, @Body() dto): Card {
        return this.cardService.updateCard(id, dto);
    }

    @Delete('/cards/:id')
    deleteCard(@Param('id') id): boolean {
        return this.cardService.deleteCard(id);
    }

}

