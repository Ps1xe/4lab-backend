import { Column } from 'src/columns/entities/column.entity';
import { BaseEntity } from 'src/common/entities/base.entity';
import { IsString } from 'class-validator';

export class Card extends BaseEntity {
  
  @IsString()
  id: string;

  @IsString()
  columnId: Column['id'];

  @IsString()
  name: string;
}
