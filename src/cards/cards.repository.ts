import { randomUUID } from "crypto";
import { Card } from "./entities/card.entity";

export class CardRepository {
    private cards: Card[] = [];

    findAll(): Card[] {
        return this.cards;
    }

    findOne(id: string): Card {
        return this.cards.find(card => card.id === id);
    }

    createOne(partialEntity: Partial<Card>): Card {
        let card = {
            id: randomUUID(),
            updatedAt: new Date().toISOString(),
            createdAt: new Date().toISOString(),
            ...partialEntity
        } as Card;
        this.cards.push(card);
        return card;
    }

    updateOne(id: string, partialEntity: Partial<Card>): Card {
        let index = this.cards.findIndex(card => card.id === id);
        let card = {
            ...this.cards[index],
            updatedAt: new Date().toISOString(),
            ...partialEntity
        }
        this.cards.splice(index, 1, card)
        return card;
    }

    deleteOne(id: string): boolean {
        try {
            let index = this.cards.findIndex(card => card.id === id);
            this.cards.splice(index, 1);
            return true;
        } catch (e) {
            false
        }
    }

    allCardsFindColumn(id: string): Card[] {      
        return this.cards.filter(card => card.columnId === id);
    }

}
