import { Injectable } from '@nestjs/common'
import { Column } from 'src/columns/entities/column.entity';
import { CardRepository } from './cards.repository'
import { CreateCardDto } from './dto/create-card.dto';
import { UpdateCardDto } from './dto/update-card.dto';
import { Card } from './entities/card.entity'

@Injectable()
export class CardService {
    constructor(private cardRepostory: CardRepository) { }

    getCards(): Card[] {
        return this.cardRepostory.findAll();
    }

    getCard(id: Card['id']): Card {
        return this.cardRepostory.findOne(id);
    }

    createCard(dto: CreateCardDto): Card {
        return this.cardRepostory.createOne(dto);
    }

    updateCard(id: Card['id'], dto: UpdateCardDto): Card {
        return this.cardRepostory.updateOne(id, dto);
    }


    deleteCard(id: Card['id']): boolean {
        return this.cardRepostory.deleteOne(id);
    }

    findCardsOnColumn(id: Column['id']): Card[] {
        return this.cardRepostory.allCardsFindColumn(id);
    }
}