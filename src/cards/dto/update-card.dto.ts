import { IsString } from "class-validator";
import { Column } from "src/columns/entities/column.entity";

export class UpdateCardDto {

    @IsString()
    name?: string;

    @IsString()
    columnId?: Column['id'];
}
