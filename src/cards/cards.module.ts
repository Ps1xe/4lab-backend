import { Module } from "@nestjs/common";
import { CardControler } from "./cards.controller";
import { CardRepository } from "./cards.repository";
import { CardService } from "./cards.service";


@Module({
    controllers: [CardControler],
    providers: [CardService, CardRepository]
})
export class CardsModule { }