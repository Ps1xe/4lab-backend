import { IsString } from 'class-validator';
import { BaseEntity } from 'src/common/entities/base.entity';

export class Column extends BaseEntity {

  @IsString()
  id: string;

  @IsString()
  name: string;
}
