import { Module } from "@nestjs/common";
import { ColumnControler } from "./columns.controller";
import { ColumnRepository } from "./columns.repository";
import { ColumnService } from "./columns.service";



@Module({
    controllers: [ColumnControler],
    providers: [ColumnService, ColumnRepository]
})
export class ColumnsModule { }