import { randomUUID } from "crypto";
import { Column } from "./entities/column.entity";

export class ColumnRepository {
    private columns: Column[] = [];

    findAll(): Column[] {
        return this.columns;
    }

    findOne(id: string): Column {
        return this.columns.find(column => column.id === id);
    }

    createOne(partialEntity: Partial<Column>): Column {
        let column = {
            id: randomUUID(),
            updatedAt: new Date().toISOString(),
            createdAt: new Date().toISOString(),
            ...partialEntity
        } as Column;
        this.columns.push(column);
        return column;
    }

    updateOne(id: string, partialEntity: Partial<Column>): Column {
        let index = this.columns.findIndex(column => column.id === id);
        let column = {
            ...this.columns[index],
            updatedAt: new Date().toISOString(),
            ...partialEntity
        }
        this.columns.splice(index, 1, column)
        return column;
    }

    deleteOne(id: string): boolean {
        try {
            let index = this.columns.findIndex(column => column.id === id);
            this.columns.splice(index, 1);
            return true;
        } catch (e) {
            false
        }
    }


}
