import { Injectable } from "@nestjs/common";
import { ColumnRepository } from "./columns.repository";
import { CreateColumnDto } from "./dto/create-column.dto";
import { UpdateColumnDto } from "./dto/update-column.dto";
import { Column } from "./entities/column.entity";



@Injectable()
export class ColumnService {
    constructor(private columnRepostory: ColumnRepository) { }

    getColumns(): Column[] {
        return this.columnRepostory.findAll();
    }

    getColumn(id: Column['id']): Column {
        return this.columnRepostory.findOne(id);
    }

    createColumn(dto: CreateColumnDto): Column {
        return this.columnRepostory.createOne(dto);
    }

    updateColumn(id: Column['id'], dto: UpdateColumnDto): Column {
        return this.columnRepostory.updateOne(id, dto);
    }


    deleteColumn(id: Column['id']): boolean {
        return this.columnRepostory.deleteOne(id);
    }

}