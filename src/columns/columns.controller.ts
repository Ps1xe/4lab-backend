import { Body, Controller, Delete, Get, Param, Patch, Post } from '@nestjs/common'
import { ColumnService } from './columns.service';
import { Column } from './entities/column.entity';

@Controller('columns')
export class ColumnControler {
    constructor(private columnService: ColumnService) { }

    @Get()
    getAllColumns(): Column[] {
        return this.columnService.getColumns();
    }

    @Get(':id')
    getOneColumn(@Param('id') id): Column {
        return this.columnService.getColumn(id);
    }

    @Post()
    createColumn(@Body() dto): Column {
        return this.columnService.createColumn(dto)
    }
    @Patch(':id')
    updateColumn(@Param('id') id, @Body() dto): Column {
        return this.columnService.updateColumn(id, dto)
    }

    @Delete(':id')
    deleteColumn(@Param('id') id): boolean {
        return this.columnService.deleteColumn(id)
    }



}